
Source: avro-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends: ant,
               debhelper-compat (= 12),
               default-jdk,
               junit4,
               libcommons-compress-java,
               libcommons-lang-java,
               libjackson-json-java,
               libjavacc-maven-plugin-java,
               libmaven-bundle-plugin-java,
               libmaven-file-management-java,
               libmaven-plugin-testing-java,
               libmaven-plugin-tools-java (>= 3.2),
               libmaven3-core-java,
               libparanamer-java (>= 2.3),
               libparanamer-maven-plugin-java,
               libslf4j-java,
               libsnappy-java,
               libxz-java,
               maven-debian-helper (>= 1.5),
               thrift-compiler,
               velocity,
               libmaven-shade-plugin-java
Standards-Version: 4.4.1
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/java-team/avro-java.git
Vcs-Browser: https://salsa.debian.org/java-team/avro-java
Homepage: http://avro.apache.org

Package: libavro-java
Architecture: all
Depends: ${misc:Depends}, libjackson-json-java, libparanamer-java, libsnappy-java, libcommons-compress-java, libxz-java, libslf4j-java
Description: Apache Avro data serialization system
 Apache Avro is a data serialization system providing:
  * Rich data structures.
  * A compact, fast, binary data format.
  * A container file, to store persistent data.
  * Remote procedure call (RPC).
  * Simple integration with dynamic languages. Code generation is not required
    to read or write data files nor to use or implement RPC protocols. Code
    generation as an optional optimization, only worth implementing
    for statically typed languages.
 .
 This package contains the Java API of Apache Avro

Package: libavro-compiler-java
Architecture: all
Depends: ${misc:Depends}, libavro-java, libcommons-lang-java, velocity
Description: Apache Avro compiler for Java
 Apache Avro is a data serialization system providing:
  * Rich data structures.
  * A compact, fast, binary data format.
  * A container file, to store persistent data.
  * Remote procedure call (RPC).
  * Simple integration with dynamic languages. Code generation is not required
    to read or write data files nor to use or implement RPC protocols. Code
    generation as an optional optimization, only worth implementing
    for statically typed languages.
 .
 This package contains the compilers for Avro IDL and Avro specific Java API.

Package: libavro-maven-plugin-java
Architecture: all
Depends: ${misc:Depends}, libavro-compiler-java, libmaven-file-management-java
Description: Apache Avro Maven plugin
 Apache Avro is a data serialization system providing:
  * Rich data structures.
  * A compact, fast, binary data format.
  * A container file, to store persistent data.
  * Remote procedure call (RPC).
  * Simple integration with dynamic languages. Code generation is not required
    to read or write data files nor to use or implement RPC protocols. Code
    generation as an optional optimization, only worth implementing
    for statically typed languages.
 .
 This package contains the Maven plugin for Avro IDL and specific API compilers
